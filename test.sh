#!/bin/sh
set -e

cargo build
gcc -Wall test.c -o test  -L./target/debug/ -lcbindgen_warmup
LD_LIBRARY_PATH=./target/debug  ./test
