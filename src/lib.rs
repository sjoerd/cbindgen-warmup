use std::{ffi::CStr, io::Write, os::raw::c_char};

#[no_mangle]
pub unsafe extern "C" fn print_str(s: *const c_char) {
    let s = if s.is_null() {
        "<NULL>\n".into()
    } else {
        CStr::from_ptr(s).to_string_lossy()
    };
    let mut output = std::io::stdout();

    write!(output, "> {}", s);
    output.flush();
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
